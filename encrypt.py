import os
import time
import json
from cryptography.fernet import Fernet
from Cryptodome.Cipher import PKCS1_OAEP
from Cryptodome.PublicKey import RSA
import base64

def get_job_details():
    """Reads in metadata information about assets used by the algo"""
    job = dict()
    job['dids'] = json.loads(os.getenv('DIDS', None))
    job['metadata'] = dict()
    job['files'] = dict()
    job['algo'] = dict()
    job['secret'] = os.getenv('secret', None)
    algo_did = os.getenv('TRANSFORMATION_DID', None)
    if job['dids'] is not None:
        for did in job['dids']:
            # get the ddo from disk
            filename = '/data/ddos/' + did
            print(f'Reading json from {filename}')
            with open(filename) as json_file:
                ddo = json.load(json_file)
                # search for metadata service
                for service in ddo['services']:
                    if service['type'] == 'compute':
                        job['files'][did] = list()
                        index = 0
                        for file in service['files']:
                            job['files'][did].append(
                                '/data/inputs/' + did + '/' + str(index))
                            index = index + 1
    if algo_did is not None:
        job['algo']['did'] = algo_did
        job['algo']['ddo_path'] = '/data/ddos/' + algo_did
    return job


def encrypt(job_details):
    """Executes encryption for the inputs"""
    print('Starting encryption job with the following input information:')
    print(json.dumps(job_details, sort_keys=True, indent=4))

    first_did = job_details['dids'][0]
    filename = job_details['files'][first_did][0]
    
    skey = Fernet.generate_key()
    symkey = Fernet(skey)

    certificate= "/app/x509CertificateChain.pem"
   # certifiate= "x509CertificateChain.pem"

    #get public key from certifcate
    pub_key = RSA.importKey(open(certificate).read())
    # creyte cyper
    cipher = PKCS1_OAEP.new(pub_key)
    # encrypt symetric key
    cipher_text = cipher.encrypt(skey)

    print('Encrypted key, writing to ' + filename)
    # write encrypted symkey
    crypto_b64 = base64.b64encode(cipher_text)

    with open("/data/outputs/key_encrypted", "wb") as file:
        file.write(crypto_b64 )

    print('Encrypting file ' + filename)

    with open(filename, "rb") as file:
        file_data = file.read()
        encrypted_data = symkey.encrypt(file_data)
    
    out_file = "data/outputs/encrypted.jpg"
    print('Encrypted file writing encrypted.jpg')
    with open(out_file, "wb") as file:
        file.write(encrypted_data)

if __name__ == '__main__':
    encrypt(get_job_details())
